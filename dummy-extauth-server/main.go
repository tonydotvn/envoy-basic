package main

import (
	"context"
	"flag"
	core "github.com/envoyproxy/go-control-plane/envoy/api/v2/core"
	authPb "github.com/envoyproxy/go-control-plane/envoy/service/auth/v2"
	_type "github.com/envoyproxy/go-control-plane/envoy/type"
	"github.com/gogo/googleapis/google/rpc"
	"google.golang.org/genproto/googleapis/rpc/status"
	"google.golang.org/grpc"
	"log"
	"net"
	"strings"
)

type authorizationService struct{}

// Check the faked checker, it will take a bearer token format field_1=value_1;field_2=value_2
// and pass back respective headers 
// x-ext-auth-field_1: value_1
// x-ext-auth-field_2: value_2
func (s *authorizationService) Check(ctx context.Context, req *authPb.CheckRequest) (*authPb.CheckResponse, error) {
	authHeader, ok := req.Attributes.Request.Http.Headers["authorization"]
	var splitToken []string
	if ok {
		splitToken = strings.Split(authHeader, "Bearer ")
	}
	invalidResponse := &authPb.CheckResponse{
		Status: &status.Status{
			Code:    int32(rpc.UNAUTHENTICATED),
			Message: "Unauthorized",
		},
		HttpResponse: &authPb.CheckResponse_DeniedResponse{
			DeniedResponse: &authPb.DeniedHttpResponse{
				Status:  &_type.HttpStatus{Code: _type.StatusCode_Unauthorized},
				Headers: nil,
				Body:    "#secure: please pass a token format field_1=value_1;field_2=value_2",
			},
		},
	}

	if len(splitToken) < 2 {
		return invalidResponse, nil
	}

	token := splitToken[1]
	headerPrefix := "x-ext-auth-"
	var headers []*core.HeaderValueOption
	for _, pair := range strings.Split(token, ";") {
		parts := strings.Split(pair, "=")
		if len(parts) != 2 {
			continue
		}
		headers = append(headers, &core.HeaderValueOption{Header: &core.HeaderValue{
			Key:   headerPrefix + parts[0],
			Value: parts[1],
		}})
	}

	return &authPb.CheckResponse{
		Status: &status.Status{Code: int32(rpc.OK)},
		HttpResponse: &authPb.CheckResponse_OkResponse{
			OkResponse: &authPb.OkHttpResponse{
				Headers: headers,
			},
		},
	}, nil
}

var (
	listenAddr string
)

type server struct {
	listener   net.Listener
	grpcServer *grpc.Server
	service    *authorizationService
}

func (s *server) start(listenAddress string) error {
	var err error
	s.listener, err = net.Listen("tcp", listenAddress)
	if err != nil {
		return err
	}

	s.service = &authorizationService{}
	s.grpcServer = grpc.NewServer()
	authPb.RegisterAuthorizationServer(s.grpcServer, s.service)

	return s.grpcServer.Serve(s.listener)
}

func main() {
	s := new(server)

	flag.StringVar(&listenAddr, "listen", ":4000", "listening address")
	flag.Parse()

	log.Printf("listening on %s", listenAddr)

	if err := s.start(listenAddr); err != nil {
		log.Fatalf("failed to start: %v", err)
	}
}
